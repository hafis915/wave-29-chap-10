function changeName(name) {
  return (dispatch, getValue) => {
    dispatch({
      type: "CHANGE_NAME",
      payload: name,
      test: "testsets",
    });
  };
}

function changeAge(num) {
  return (dispatch, getValue) => {
    dispatch({
      type: "CHANGE_AGE",
      payload: num,
    });
  };
}

module.exports = {
  changeName,
  changeAge,
};
