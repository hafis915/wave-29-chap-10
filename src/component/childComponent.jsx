import SubChildComponent from "./subchildComponent";
import { changeName, changeAge } from "../actions";
import { useDispatch, useSelector } from "react-redux";

export default function ChildComponent() {
  const dispatch = useDispatch();
  const { name, age } = useSelector((state) => state.userData);

  function ubahNama() {
    dispatch(changeAge(8));
    if (name === "hafis") {
      dispatch(changeName("UBAH"));
    } else {
      dispatch(changeName("hafis"));
    }
  }
  return (
    <div className="">
      <h1>CHILD COMPONENT</h1>
      <button onClick={ubahNama}>UBAH NAMA</button>
      <SubChildComponent />
      <h1>{age}</h1>
    </div>
  );
}
