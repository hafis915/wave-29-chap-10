import { useSelector } from "react-redux";

export default function SubChildComponent() {
  const { name } = useSelector((state) => state.userData);
  return (
    <div className="">
      <h1> {name} SUBCHILD COMP</h1>
    </div>
  );
}
