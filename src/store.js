import { applyMiddleware, createStore, combineReducers } from "redux";
import userReducer from "./reducers";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  userData: userReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
