import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import ChildComponent from "./component/childComponent";

// useDispatch => berkomunikasi dengan action
// useSelector => berkomunikasi dengan reducer
function App() {
  const data = useSelector((state) => state);
  console.log(data.userData)
  return (
    <div className="App">
      <h1> ini dari APP js</h1>
      <ChildComponent />
    </div>
  );
}

export default App;
